/**
 * /**
 * @file Archivo que contiene el módulo ova-api
 * @namespace index
 * @module ova-api
 * @author Christian Arias <cristian1995amr@gmail.com>
 */


/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args)){
        args = {}
    }    

    const components = {
        concepts: require('./concepts/index.js'),
        media: require('./media/index.js'),
        finishedTopics: require('./finishedTopics/index.js'),
        utils: require('./utils/index.js'),
        course: require('./course/index.js')
    } 

    if(args.defaultMethodWrapper === false){
        this.components = components;  
    }
    else{
        _.merge(this, require('./API/defaultMethodWrapper.js'));
    }
}
    