const _ = require('lodash');
const conceptsHandler = require('../concepts/conceptsHandler.js')
const mediaHandler = require('../media/mediaHandler.js')
const courseHandler = require('../course/courseHandler.js')
const finishedTopicsHandler = require('../finishedTopics/finishedTopicsHandler.js')
const teamHandler = require('../team/teamHandler.js')
const queryHandler = require('../API/queryHandler.js')

module.exports = function (args) {
    args = _.isObject(args) ? args : {};
    if(!_.isObject(args.arguments)){
        args.arguments = {}
    }
    var queries = {
        concepts: conceptsHandler(_.merge({}, args, {
            method: 'getLOConcepts',
            returnQuery: true
        })),
        media: mediaHandler(_.merge({}, args, {
            method: 'getLOMedia',
            returnQuery: true
        })),
        finishedTopics: finishedTopicsHandler(_.merge({}, args, {
            method: 'getLOFinishedTopics',
            returnQuery: true
        })),
        courseActivities: courseHandler(_.merge({}, args , {
            method: 'getCourseActivities',
            returnQuery: true
        })),
        team: teamHandler(_.merge({}, args, {
            method: 'getLOTeam',
            returnQuery: true
        }))
    };

    return queryHandler({
        showFeedback: args.showFeedback,
        returnQuery: args.returnQuery,
        queries: queries
    });
}