const _ = require('lodash');
const moodleCookie = require('../API/moodleCookie.js');
const queryHandler = require('../API/queryHandler.js');

module.exports = function (args) {
    if(!_.isObject(args)){
        args = {}
    }
    
    return queryHandler(_.merge({}, args, {
        method: 'getUserId',
        arguments: _.merge({}, args.arguments, {
            sessionId: moodleCookie.get(args)
        })
    }));
}