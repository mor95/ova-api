const _ = require('lodash');
const queryHandler = require('../API/queryHandler.js');
const checkTypes = require('check-types');

module.exports = function (args) {
    args = _.isObject(args) ? args : {};
    if(!_.isObject(args.arguments)){
        args.arguments = {}
    }
    checkTypes.assert.object(args);
    checkTypes.assert.string(args.arguments.shortName);
    
    return queryHandler(_.merge({}, args, {
        method: 'getUserId',
        arguments: _.merge({}, args.arguments, {
            sessionId: moodleCookie.get(args)
        })
    }));
}