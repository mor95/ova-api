const _ = require('lodash');
const API = require('./API.js');

module.exports = function (args) {
    if (!_.isObject(args)) {
        args = {};
    }

     this.arguments = args;
    
    _.merge(this, API.getAPILocationSources(args));

    if(args.returnQuery === true){
        return this.arguments;
    }

    this.defaultRequestType = 'GET';
    if(typeof args.requestType === 'string'){
        this.requestType = args.requestType;
    }
    else{
        this.requestType = this.defaultRequestType;
    }

    if(this.requestType === 'GET'){
        return fetch(this.APILocation + '?' + $.param(this.arguments), {
            method: this.requestType
        });
    }
    else{
        return fetch(this.APILocation, {
            method: this.requestType,
            body: JSON.stringify(this.arguments)
        });
    }
}